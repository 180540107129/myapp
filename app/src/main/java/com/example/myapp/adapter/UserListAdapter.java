package com.example.myapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapp.R;
import com.example.myapp.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;


public class UserListAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, Object>> userList;

    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        @SuppressLint({"ViewHolder", "InflateParams"}) View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);
        TextView tvName = view1.findViewById(R.id.tvLstName);
        TextView tvPhone = view1.findViewById(R.id.tvLstEmail);
        TextView tvEmail = view1.findViewById(R.id.tvLstPhone);
        TextView tvGender = view1.findViewById(R.id.tvLstGender);


        tvName.setText(String.valueOf(userList.get(position).get(Constant.USER_NAME)));
        tvEmail.setText(String.valueOf(userList.get(position).get(Constant.EMAIL_ADDRESS)));
        tvPhone.setText(String.valueOf(userList.get(position).get(Constant.MOBILE_NUMBER)));
        tvGender.setText(String.valueOf(userList.get(position).get(Constant.GENDER)));

        if (String.valueOf(userList.get(position).get(Constant.GENDER)).equals("Male")) {
            tvGender.setText("M");
            tvGender.setBackground(context.getResources().getDrawable(R.drawable.male_bg));
        } else if (String.valueOf(userList.get(position).get(Constant.GENDER)).equals("Female")) {
            tvGender.setText("F");
            tvGender.setBackground(context.getResources().getDrawable(R.drawable.female_bg));
        }
        return view1;

    }
}
