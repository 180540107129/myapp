package com.example.myapp.util;

public class Constant {

    public static final String USER_NAME = "userName";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String GENDER = "gender";
    public static final String HOBBY = "hobby";
}
