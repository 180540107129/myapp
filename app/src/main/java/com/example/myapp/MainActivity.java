package com.example.myapp;


import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapp.database.MyDatabase;
import com.example.myapp.database.TblUserData;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {

    EditText etUserName, etPhoneNumber, etEmail;
    Button btnSubmit;
    Button btnNameToast;
    Button btnNumToast;
    Button btnFetch;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootball;
    CheckBox chbHockey;
    CheckBox chbSwimming;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewBind();
        initViewEvent();
        ChbValidation();
        btnToastAll();

    }


    private void initViewEvent() {

        btnFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TblUserData tblUserData = new TblUserData(MainActivity.this);
                Cursor c = tblUserData.fetchUsers();
                while (c.isAfterLast() == false) {
                    Log.e("LOG:::Himanshu", "Name: " + c.getString(c.getColumnIndex("Name")) + " Phone No.: " + c.getString(c.getColumnIndex("PhoneNumber")));
                    c.moveToNext();
                }
                Toast.makeText(getApplicationContext(), "Please check logcat to see db entries", Toast.LENGTH_SHORT).show();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    String name = etUserName.getText().toString();
                    String phoneNumber = etPhoneNumber.getText().toString();
                    String email = etEmail.getText().toString();
                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertedId = tblUserData.insertUserDetail(name, phoneNumber, email);
                    Toast.makeText(getApplicationContext(), lastInsertedId > 0 ?
                            "User Inserted Successfully" : "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                    chbSwimming.setVisibility(View.VISIBLE);
                    chbCricket.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.GONE);
                    chbSwimming.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                    chbFootball.setVisibility(View.VISIBLE);


                }
            }
        });

    }

    private void ChbValidation() {

        if (R.id.rbActFemale == rgGender.getCheckedRadioButtonId()) {
            chbCricket.setVisibility(View.GONE);
            chbFootball.setVisibility(View.GONE);
            chbHockey.setVisibility(View.VISIBLE);
        }

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.GONE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void btnToastAll() {

        btnNameToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etUserName.getText().toString();
                Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
            }
        });


        btnNumToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num = etPhoneNumber.getText().toString();
                Toast.makeText(getApplicationContext(), num, Toast.LENGTH_LONG).show();
            }
        });
    }

    boolean isValid() {
        boolean flag = true;

        if (TextUtils.isEmpty(etUserName.getText())) {
            etUserName.setError("Enter UserName");
            etUserName.setCursorVisible(true);
            flag = false;
        }
        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            etPhoneNumber.setError("Please enter phone number");
            etPhoneNumber.setCursorVisible(true);
            flag = false;
        } else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() < 10 || phoneNumber.contains(" ")) {
                etPhoneNumber.setError("Please enter a valid phone number");
                etPhoneNumber.setCursorVisible(true);
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            etEmail.setError("Please enter email address");
            etEmail.setCursorVisible(true);
            flag = false;
        } else {
            String email = etEmail.getText().toString();
            String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";

            if (!(email.matches(emailPattern))) {
                etEmail.setError("Please enter valid email address");
                etEmail.setCursorVisible(true);
                flag = false;
            }
        }
        if (!(rbMale.isChecked()) && !(rbFemale.isChecked())) {
            Toast.makeText(this, "Please enter your gender", Toast.LENGTH_LONG).show();
            flag = false;
        }

        return flag;
    }


    @SuppressLint("SetTextI18n")
    private void initViewBind() {
        new MyDatabase(MainActivity.this).getReadableDatabase();
        etUserName = findViewById(R.id.etUserName);
        etPhoneNumber = findViewById(R.id.etActNumber);
        etEmail = findViewById(R.id.etActEmail);
        btnSubmit = findViewById(R.id.btnActSubmit);
        btnNameToast = findViewById(R.id.btnActNameToast);
        btnNumToast = findViewById(R.id.btnActNumToast);
        btnFetch = findViewById(R.id.btnFetch);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbCricket = findViewById(R.id.chbActCricket);
        chbFootball = findViewById(R.id.chbActFootball);
        chbHockey = findViewById(R.id.chbActHockey);

        getSupportActionBar().setTitle(R.string.lbl_main_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.new_game) {

        } else if (item.getItemId() == R.id.help) {

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}

